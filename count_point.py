import scipy.misc as misc
import scipy as sp
import numpy as np

def getWidthAndCount(image):
    """
    INPUT - data Matrix; Shape - N * M * 4
    RETURN - dict with key:value - width: count_width; count black Pixels
    """
    new_image = np.zeros(image.shape)
    count_black_pix = 0
    w_point = {}
    for i in range(image.shape[0]):
        w = 0
        for j in range(image.shape[1]):
            if sum(image[i][j]) == 1020:
                new_image[i][j] = np.array([255, 255, 255, 255])
                if w!= 0:
                    if w in w_point:
                        w_point[w] += 1
                    else:
                        w_point[w] = 1
                w = 0
            else:
                new_image[i][j] = np.array([0, 0, 0, 255])
                count_black_pix += 1
                w+=1
    return w_point, count_black_pix

def getOftenWidth(width_dict):
    """
    Find the most frequent value of width
    """
    often_width = 0
    width = 0
    for k in width_dict:
        if width_dict[k] > often_width:
            often_width = width_dict[k]
            width = k
    return width

# read image
image = misc.imread('test.png')

# Variant 1
dictWidth_1, countBlackPix_1 = getWidthAndCount(image)
width = getOftenWidth(dictWidth_1)
countPoints_1 = countBlackPix_1 / width**2
# Variant 2
dictHeigth_2, countBlackPix_2 = getWidthAndCount(image.transpose((1,0,2)))
heigth = getOftenWidth(dictHeigth_2)
countPoints_2 = countBlackPix_2 / heigth**2
#Varaiant 3
countPoints_3 = countBlackPix_2 / (width*heigth)

average_count = np.mean([countPoints_1, countPoints_2, countPoints_3])
print (average_count)
delta = countBlackPix_1 * np.sqrt(heigth**6 + width**6) / (heigth**3 * width**3)
print (delta)
