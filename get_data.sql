SELECT u.id, date_part('month', p.date) as month, AVG(i.price)
FROM "Purchases" p
LEFT JOIN "Items" i
  ON p.item_id = i.items_id
LEFT JOIN "Users" u
  ON p.user_id = u.id
GROUP BY  date_part('month', p.date), u.id
ORDER BY u.id
