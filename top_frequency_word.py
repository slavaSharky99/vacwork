import requests
from bs4 import BeautifulSoup
import time
from datetime import datetime
import re

from pytagcloud import create_tag_image, make_tags
from pytagcloud.lang.counter import get_tag_counts

def last_day_of_month(year, month):
    last_days = [31, 30, 29, 28, 27]
    for i in last_days:
        try:
            end = datetime(year, month, i)
        except ValueError:
            continue
        else:
            return end.day
    return None
useragent = {'User-Agent': 'Mozilla/5.0 (compatible; MSIE 10.6; Windows NT 6.1; Trident/5.0; InfoPath.2; SLCC1; .NET CLR 3.0.4506.2152; .NET CLR 3.5.30729; .NET CLR 2.0.50727) 3gpp-gba UNTRUSTED/1.0'}
# Find Posts with query = Russia
r = requests.get('https://news.google.com/search?q=Russia&hl=en-US&gl=US&ceid=US%3Aen', headers=useragent)

soup = BeautifulSoup(r.text, 'html.parser')
time_now = int(time.time())
# days = last_day_of_month(datetime.now().year, datetime.now().month)
days = 3

# top words - word: count
top_words = {}
regex = re.compile(r"[a-z]+")
for i in soup.find_all('div', class_='xrnccd'):

    # find time post
    post_time = i.find('div', {'class':'QmrVtf kybdz'}).\
                  find('div',{'class':'SVJrMe'}).find('time')['datetime'].\
                  split(': ')[1]
    post_time = int(post_time)
    href = i.find('article').find('a')['href']
    delta = datetime.fromtimestamp(time_now) - datetime.fromtimestamp(post_time)
    if days >= delta.days:
        post_link = 'https://news.google.com'+href[1:]
        post_ = requests.get(post_link)
        soup_ = BeautifulSoup(post_.text, 'html.parser')
        for p in soup_.find_all('p'):
            for w in p.text.split(' '):
                if w.lower() in ['with', 'on','for','that','is', 'a', 'in',
                                        'to', 'of', 'and', 'the', 'at', 'it',
                                        'an', 'are', 'is', 'as']:
                    continue
                if len(regex.findall(w.lower())) > 0:
                    if w.lower() in top_words:
                        top_words[w.lower()] += 1
                    else:
                        top_words[w.lower()] = 1
    time.sleep(0.5)
    # if len(top_words) > 1000:
    #     print (top_words)
    #     break

top_words_final = [['randomValue', 0]]
for k in top_words:
    if len(top_words_final) < 50:
        for i in range(len(top_words_final)):
            if top_words_final[i][1] > top_words[k]:
                top_words_final.insert(i, [k, top_words[k]])
                break
        if top_words[k] >= top_words_final[len(top_words_final)-1][1]:
            top_words_final.append([k, top_words[k]])

    else:
        if top_words_final[0][1] > top_words[k]:
            continue
        for i in range(len(top_words_final)):
            if top_words_final[i][1] > top_words[k]:
                top_words_final.insert(i, [k, top_words[k]])
                break
        del top_words_final[0]
        if top_words[k] >= top_words_final[len(top_words_final)-1][1]:
            top_words_final.append([k, top_words[k]])


counts = top_words_final
tags = make_tags(counts, maxsize=120)

create_tag_image(tags, 'cloud_large.png', size=(1800, 1200), fontname='Lobster')
